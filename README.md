**Madison emergency dentist**

If you or a loved one ever found yourself in the midst of a sudden situation dealing with teeth issues, the first thing to know is not to worry. 
The Oral Health Associates of Madison are pleased to help. 
Contact our Madison WI Emergency Dentist instantly during normal business hours, even if the emergency occurs 
after our business hours, we will help you with our emergency dental services.
Simply follow the directions on our office phone line to be connected to an emergency dentist who is on call. 
For 24 hours, the dentist can triage the condition and aid with deciding the correct course of action.
Please Visit Our Website [Madison emergency dentist](https://dentistmadisonwi.com/emergency-dentist.php) for more information. 

---

## Our emergency dentist in Madison 

Sometimes, after a tooth slips, people feel little, while others report extreme discomfort. 
To clean your teeth, stay cool and use warm water. Spit the water out into a cup, to see if any pieces of the tooth fell out. 
Take pictures of the issue area and notify our office shortly afterwards.
The more information you can get over the phone, the easier, and you can send us photographs of the issue. 
Missing or cracked teeth that cause pain would usually require a 24-hour dentist to have immediate, same-day care.
Lost or chipped teeth that don't inflict discomfort can be seen at a later stage. In non-emergency cases, dentists 
will also make two appointments: one for a thorough consultation to determine the extent of the condition and a second for maintenance work. 
For more information on these and other dental emergency situations in Madison, WI, email us today at Dental Health Associates of Madison.
Don't wait until it's too late to arrive in the Madison area at one of our Madison WI Emergency Dentist clinics. 
Our Oral Health Associates focus on patching chipped teeth in Middleton, Downtown Madison, Fitchburg and more.

